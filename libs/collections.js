import {Mongo} from 'meteor/mongo';

export default {
  Exercises: new Mongo.Collection('exercises'),
  Plans: new Mongo.Collection('plans'),
  Trainings: new Mongo.Collection('trainings')
};
