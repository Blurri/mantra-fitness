import {Plans,Exercises} from '/libs/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
  Meteor.methods({
    'plans.create'(_id, title) {
      check(_id, String);
      check(title, String);


      const createdAt = new Date();
      const plan = {
        _id, title, createdAt,exercises : [],
        saving: true
      };
      Plans.insert(plan);
    },
    'plans.update'(_id,title, exercises) {
      check(_id, String);
      check(title, String);

      const updatedAt = new Date();
      const plan = {
        title,updatedAt,exercises,
        saving: true,
      };
      Plans.update(_id, {$set : plan})
    },
    'plans.addexercise'(planId,exerciseId) {
      check(planId, String);
      check(exerciseId, String);

      Plans.update(planId, {$addToSet : {exercises : exerciseId}}, (err, count) => {
        if(err){return err;};
        Exercises.update(exerciseId, {$addToSet : {plans : planId}});
      });
    },
    'plans.removeexercise'(planId,exerciseId) {
      check(planId, String);
      check(exerciseId, String);
      Plans.update(planId, {$pull : {exercises : exerciseId}}, (err,count) => {
        if(err){return err;};
        Exercises.update(exerciseId, {$pull : {plans : planId}});
      });
    }
  });
}
