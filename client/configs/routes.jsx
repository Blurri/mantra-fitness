import React from 'react';
import {injectDeps} from 'react-simple-di';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {mount} from 'react-mounter';
import MainLayout from '../components/layout.main.jsx';

import PlanList from '../containers/plans/planlist';
import Plan from '../containers/plans/plan';
import NewPlan from '../containers/plans/newplan';
import EditPlan from '../containers/plans/editplan';

import ExerciseList from '../containers/exercises/exerciselist';
import Exercise from '../containers/exercises/exercise';
import NewExercise from '../containers/exercises/newexercise';
import EditExercise from '../containers/exercises/editexercise';

import Training from '../containers/trainings/training';

export const initRoutes = (context, actions) => {
  const MainLayoutCtx = injectDeps(context, actions)(MainLayout);
  // Move these as a module and call this from a main file

  FlowRouter.route('/', {
    name: 'plans.list',
    triggersEnter: [function() {DocHead.setTitle('Planlist');}],
    action() {
      mount(MainLayoutCtx, {
        content: () => (<PlanList />)
      });
    }
  });

  FlowRouter.route('/plans/:planId', {
    name: 'plans.single',
    triggersEnter: [function() {DocHead.setTitle('Plan');}],
    action({planId}) {
      mount(MainLayoutCtx, {
        content: () => (<Plan planId={planId}/>)
      });
    }
  });

  FlowRouter.route('/new-plan', {
    name: 'newplan',
    triggersEnter: [function() {DocHead.setTitle('New Plan');}],
    action() {
      mount(MainLayoutCtx, {
        content: () => (<NewPlan/>)
      });
    }
  });



  FlowRouter.route('/edit-plan/:planId', {
    name: 'edit-plan',
    triggersEnter: [function() {DocHead.setTitle('Edit Plan');}],
    action({planId}) {
      mount(MainLayoutCtx, {
        content: () => (<EditPlan planId={planId}/>)
      });
    }
  });


  // Exercises
  FlowRouter.route('/exercises', {
    name: 'exercises.list',
    triggersEnter: [function() {DocHead.setTitle('Exerciselist');}],
    action() {
      mount(MainLayoutCtx, {
        content: () => (<ExerciseList />)
      });
    }
  });

  FlowRouter.route('/exercises/:exerciseId', {
    name: 'exercises.single',
    triggersEnter: [function() {DocHead.setTitle('Exercise');}],
    action({exerciseId}) {
      mount(MainLayoutCtx, {
        content: () => (<Exercise exerciseId={exerciseId}/>)
      });
    }
  });

  FlowRouter.route('/new-exercise', {
    name: 'newexercise',
    triggersEnter: [function() {DocHead.setTitle('New Exercise');}],
    action() {
      mount(MainLayoutCtx, {
        content: () => (<NewExercise/>)
      });
    }
  });

  FlowRouter.route('/edit-exercise/:exerciseId', {
    name: 'edit-exercises',
    triggersEnter: [function() {DocHead.setTitle('Edit Exercise');}],
    action({exerciseId}) {
      mount(MainLayoutCtx, {
        content: () => (<EditExercise exerciseId={exerciseId}/>)
      });
    }
  });

  // Trainings
  FlowRouter.route('/training/:planId', {
    name: 'training',
    triggersEnter: [function() {DocHead.setTitle('Training');}],
    action({planId}) {
      mount(MainLayoutCtx, {
        content: () => (<Training planId={planId}/>)
      });
    }
  });


};
