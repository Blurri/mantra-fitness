import TrainingsCard from '../../components/trainings/trainingscard.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context, exerciseId,}, onData) => {
  const {Meteor, Collections, Tracker} = context();
  Meteor.subscribe('exercises.single', exerciseId, () => {
    const exercise = Collections.Exercises.findOne(exerciseId);
    onData(null, {exercise});
  });
}

export const depsMapper = (context, actions) => ({
  updateForTraining: actions.exercises.updateForTraining,
  clearErrors: actions.trainings.clearErrors,
  context: () => context
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(TrainingsCard);
