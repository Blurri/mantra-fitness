import Training from '../../components/trainings/training.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context, planId,}, onData) => {
  const {Meteor, Collections, Tracker} = context();
    Meteor.subscribe('trainings.singleforrun', planId, () =>{
      const training = Collections.Trainings.findOne({planId : planId});
      onData(null, {planId,training});
    })
}

export const depsMapper = (context, actions) => ({
  create: actions.trainings.create,
  addFinishedExercise : actions.trainings.addFinishedExercise,
  clearErrors: actions.trainings.clearErrors,
  context: () => context
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(Training);
