import ExerciseList from '../../components/exercises/exerciselist.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();
  if (Meteor.subscribe('exercises.list').ready()) {
    const exercises = Collections.Exercises.find().fetch();
    onData(null, {exercises});
  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(ExerciseList);
