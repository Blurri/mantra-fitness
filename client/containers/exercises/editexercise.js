import EditExercise from '../../components/exercises/editexercise.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';


export const composer = ({context, exerciseId, clearErrors}, onData) => {
  const {LocalState} = context();
  const error = LocalState.get('SAVING_ERROR');
  onData(null, {error});
  const {Meteor, Collections, Tracker} = context();

  Meteor.subscribe('exercises.single', exerciseId, () => {
    const exercise = Collections.Exercises.findOne(exerciseId);
    onData(null, {exercise});
  });

  const exerciseFromCache = Tracker.nonreactive(() => {
    return Collections.Exercises.findOne(exerciseId);
  });

  if (exerciseFromCache) {
    onData(null, {exercise: exerciseFromCache});
  } else {
    onData();
  }
  return clearErrors;
};

export const depsMapper = (context, actions) => ({
  edit: actions.exercises.edit,
  clearErrors: actions.exercises.clearErrors,
  context: () => context
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(EditExercise);
