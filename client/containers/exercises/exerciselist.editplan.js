import ExerciseListEditPlan from '../../components/exercises/exerciselist.editplan.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context, planId}, onData) => {
  const {Meteor, Collections, Tracker} = context();
  if (Meteor.subscribe('exercises.list').ready()) {
    const exercises = Collections.Exercises.find().fetch();
    Meteor.subscribe('plans.single', planId, () => {
      const plan = Collections.Plans.findOne(planId);
      onData(null, {exercises,plan});
    });
  }
  const dataFromCache = Tracker.nonreactive(() => {
    const exercises = Collections.Exercises.find().fetch();
    const plan = Collections.Plans.findOne(planId)
    return {plan : plan, exercises : exercises};
  });

  if (dataFromCache) {
    onData(null, dataFromCache);
  } else {
    onData();
  }
};

export const depsMapper = (context, actions) => ({
  addExerciseToPlan: actions.exercises.addExerciseToPlan,
  removeExerciseToPlan: actions.exercises.removeExerciseToPlan,
  clearErrors: actions.exercises.clearErrors,
  context: () => context
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(ExerciseListEditPlan);
