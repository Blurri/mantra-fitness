import Exercise from '../../components/exercises/exercise.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context, exerciseId}, onData) => {
  const {Meteor, Collections, Tracker} = context();

  Meteor.subscribe('exercises.single', exerciseId, () => {
    const exercise = Collections.Exercises.findOne(exerciseId);
    onData(null, {exercise});
  });

  const exerciseFromCache = Tracker.nonreactive(() => {
    return Collections.Exercises.findOne(exerciseId);
  });

  if (exerciseFromCache) {
    onData(null, {exercise: exerciseFromCache});
  } else {
    onData();
  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(Exercise);
