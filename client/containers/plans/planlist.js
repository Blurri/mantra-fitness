import PlanList from '../../components/plans/planlist.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();
  if (Meteor.subscribe('plans.list').ready()) {
    const plans = Collections.Plans.find().fetch();
    onData(null, {plans});
  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(PlanList);
