import Plan from '../../components/plans/plan.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context, planId}, onData) => {
  const {Meteor, Collections, Tracker} = context();
  Meteor.subscribe('plans.single', planId, () => {
    const plan = Collections.Plans.findOne(planId);
    onData(null, {plan});
  });
  const planFromCache = Tracker.nonreactive(() => {
    return Collections.Plans.findOne(planId);
  });
  if (planFromCache) {
    onData(null, {plan: planFromCache});
  } else {
    onData();
  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(Plan);
