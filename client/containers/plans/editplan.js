import EditPlan from '../../components/plans/editplan.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';


export const composer = ({context, planId, clearErrors}, onData) => {
  const {LocalState} = context();
  const error = LocalState.get('SAVING_ERROR');
  onData(null, {error});

  const {Meteor, Collections, Tracker} = context();
  Meteor.subscribe('plans.single', planId, () => {
    const plan = Collections.Plans.findOne(planId);
    onData(null, {plan});
  });

  const planFromCache = Tracker.nonreactive(() => {
    return Collections.Plans.findOne(planId);
  });

  if (planFromCache) {
    onData(null, {plan: planFromCache});
  } else {
    onData();
  }
};



export const depsMapper = (context, actions) => ({
  edit: actions.plans.edit,
  clearErrors: actions.plans.clearErrors,
  context: () => context
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(EditPlan);
