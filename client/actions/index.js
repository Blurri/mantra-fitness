import plans from './plans';
import exercises from './exercises';
import trainings from './trainings';

export default {
  plans,
  exercises,
  trainings
};
