export default {
  create({Meteor, LocalState, FlowRouter}, title) {
    if (!title) {
      return LocalState.set('SAVING_ERROR', 'Title is required!');
    }

    LocalState.set('SAVING_ERROR', null);
    // LocalState.set('SAVING_NEW_PLAN', true);

    const id = Meteor.uuid();
    Meteor.call('plans.create', id, title, (err) => {
      // LocalState.set('SAVING_NEW_PLAN', false);
      if (err) {
        return LocalState.set('SAVING_ERROR', err.message);
      }
    });
    FlowRouter.go(`/plans/${id}`);
  },

  edit({Meteor, LocalState, FlowRouter},_id , title, exercises) {
      if (!title || !_id) {
        return LocalState.set('SAVING_ERROR', 'Title is required!');
      }
      LocalState.set('SAVING_ERROR', null);
      Meteor.call('plans.update', _id, title,exercises, (err) => {
        if (err) {
          return LocalState.set('SAVING_ERROR', err.message);
        }
      });
      FlowRouter.go(`/plans/${_id}`);
  },


  clearErrors({LocalState}) {
    return LocalState.set('SAVING_ERROR', null);
  }
};
