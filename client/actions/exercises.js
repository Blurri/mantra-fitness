export default {
  create({Meteor, LocalState, FlowRouter}, title, type, weight, duration, numberOfSets, repsInSet, description) {
    if (!title || !type) {
      return LocalState.set('SAVING_ERROR', 'Title and type is required!');
    }
    LocalState.set('SAVING_ERROR', null);

    const id = Meteor.uuid();
    Meteor.call('exercises.create', id, title,type , weight, duration, numberOfSets, repsInSet, description,(err) => {
      if (err) {
        return LocalState.set('SAVING_ERROR', err.message);
      }
    });
    FlowRouter.go(`/exercises/${id}`);
  },

  edit({Meteor, LocalState, FlowRouter},_id , title, type, weight, duration, numberOfSets, repsInSet, description) {
      if (!title || !type || !_id) {
        return LocalState.set('SAVING_ERROR', 'Title and type is required!');
      }
      LocalState.set('SAVING_ERROR', null);
      Meteor.call('exercises.update', _id, title,type , weight, duration, numberOfSets, repsInSet, description,(err) => {
        if (err) {
          return LocalState.set('SAVING_ERROR', err.message);
        }
      });
      FlowRouter.go(`/exercises/${_id}`);
  },

  updateForTraining({Meteor, LocalState, FlowRouter},_id , title, type, weight, duration, numberOfSets, repsInSet, description) {
    console.log('is here');
      Meteor.call('exercises.update', _id, title,type , weight, duration, numberOfSets, repsInSet, description,(err) => {
        if (err) {
        }
      });
  },

  addExerciseToPlan({Meteor, LocalState},planId, exerciseId) {
    if (!planId || !exerciseId){
      console.log('not set');
      console.log(planId, 'planid');
      console.log(exerciseId, 'exerciseId');
      return;
    }
    Meteor.call('plans.addexercise', planId, exerciseId, (err) => {
      if(err){
        console.log('not saved');
        console.log(err);
        return;
      }
    })
  },
  removeExerciseToPlan({Meteor, LocalState},planId, exerciseId) {
    if (!planId || !exerciseId){
      return;
    }
    Meteor.call('plans.removeexercise', planId, exerciseId, (err) => {
      if(err){
        return;
      }
    })
  },

  clearErrors({LocalState}) {
    return LocalState.set('SAVING_ERROR', null);
  }
};
