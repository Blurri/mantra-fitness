
export default {
  create({Meteor, LocalState}, planId) {
    Meteor.call('trainings.create', planId, (err) => {
      if (err) {return err};
    })
  },
  addFinishedExercise({Meteor, LocalState, FlowRouter},trainingId ,exerciseId){
    Meteor.call('trainings.addexercisetoplan', trainingId, exerciseId, (err) => {
      if(err) {return err};
    })
  },
  clearErrors({LocalState}) {
    return LocalState.set('SAVING_ERROR', null);
  }
};
