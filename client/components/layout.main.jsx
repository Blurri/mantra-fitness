import Navigations from './navigation/navigations.jsx';
import React from 'react';

const Layout = ({content = () => null }) => (
  <div>
    <header>
      <Navigations />
    </header>

    <div className="container">
      {content()}
    </div>

    <footer>
      <small>Fitness app</small>
    </footer>
  </div>
);

export default Layout;
