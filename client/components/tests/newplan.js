// See: ../index.js for more information
//
// import {describe, it} from 'mocha';
// import {expect} from 'chai';
// import {shallow} from 'enzyme';
// import NewPlan from '../plans/newplan.jsx';
//
// describe('components.newplan', () => {
//   it('should show the error if there are any', () => {
//     const error = 'TheError';
//     const el = shallow(<NewPlan error={error} />);
//     expect(el.html()).to.match(/TheError/);
//   });
//
//   it('should show the saving indicator, it plan is saving', () => {
//     const el = shallow(<NewPlan saving={true} />);
//     expect(el.html()).to.match(/Saving/);
//   });
//
//   it('should display the create plan form', () => {
//     const el = shallow(<NewPlan />);
//     const title = el.find('input').first();
//     const button = el.find('button').first();
//
//     expect(title.node.ref).to.be.equal('titleRef');
//     expect(button.prop('onClick')).to.be.a('function');
//   });
//
//   it('should create a new plan when click on the button', done => {
//     const title = 'the-title';
//
//     const onCreate = (t) => {
//       expect(t).to.be.equal(title);
//       done();
//     };
//
//     const el = shallow(<NewPlan create={onCreate} />);
//     const instance = el.instance();
//
//     instance.refs = {
//       titleRef: {value: title}
//     };
//
//     el.find('button').simulate('click');
//   });
// });
