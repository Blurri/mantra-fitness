import {describe, it} from 'mocha';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import Plan from '../plans/plan.jsx';

describe('components.plan', () => {
  it('should display the plan title', () => {
    const plan = {title: 'Nice One'};
    const el = shallow(<Plan plan={plan} />);
    expect(el.find('h2').text()).to.be.match(/Nice One/);
  });

  it('should display saving indicator if saving prop is there', () => {
    const plan = {saving: true};
    const el = shallow(<Plan plan={plan} />);
    expect(el.find('p').first().text()).to.be.match(/saving/i);
  });
});
