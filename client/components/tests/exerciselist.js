import {describe, it} from 'mocha';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import ExerciseList from '../exercises/exerciselist.jsx';

describe('components.exerciselist', () => {
  const exercises = [
    {title: 't-one', _id: 'one'},
    {title: 't-two', _id: 'two'},
  ];

  it('should list given number of items', () => {
    const el = shallow(<ExerciseList exercises={exercises}/>);
    expect(el.find('li').length).to.be.equal(exercises.length);
  });

  it('should list exercise title for each item', () => {
    const el = shallow(<ExerciseList exercises={exercises}/>);
    const lis = el.find('li');
    lis.forEach((li, index) => {
      const aText = li.find('a').first().text();
      expect(aText).to.be.equal(exercises[index].title);
    });
  });

  it('shallow list exercise link for each items', () => {
    const el = shallow(<ExerciseList exercises={exercises}/>);
    const lis = el.find('li');
    lis.forEach((li, index) => {
      const href = li.find('a').first().prop('href');
      expect(href).to.be.equal(`/exercises/${exercises[index]._id}`);
    });
  });
});
