import {describe, it} from 'mocha';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import PlanList from '../plans/planlist.jsx';

describe('components.planlist', () => {
  const plans = [
    {title: 't-one', _id: 'one'},
    {title: 't-two', _id: 'two'},
  ];

  it('should list given number of items', () => {
    const el = shallow(<PlanList plans={plans}/>);
    expect(el.find('li').length).to.be.equal(plans.length);
  });

  it('should list plan title for each item', () => {
    const el = shallow(<PlanList plans={plans}/>);
    const lis = el.find('li');
    lis.forEach((li, index) => {
      const aText = li.find('a').first().text();
      expect(aText).to.be.equal(plans[index].title);
    });
  });

  it('shallow list plan link for each items', () => {
    const el = shallow(<PlanList plans={plans}/>);
    const lis = el.find('li');
    lis.forEach((li, index) => {
      const href = li.find('a').first().prop('href');
      expect(href).to.be.equal(`/plans/${plans[index]._id}`);
    });
  });
});
