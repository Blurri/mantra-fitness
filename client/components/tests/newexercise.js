// // See: ../index.js for more information
//
// import {describe, it} from 'mocha';
// import {expect} from 'chai';
// import {shallow} from 'enzyme';
// import NewExercise from '../exercises/newexercise.jsx';
//
// describe('components.newexercise', () => {
//   it('should show the error if there are any', () => {
//     const error = 'TheError';
//     const el = shallow(<NewExercise error={error} />);
//     expect(el.html()).to.match(/TheError/);
//   });
//
//   it('should show the saving indicator, it exercise is saving', () => {
//     const el = shallow(<NewExercise saving={true} />);
//     expect(el.html()).to.match(/Saving/);
//   });
//
//   it('should display the create exercise form', () => {
//     const el = shallow(<NewExercise />);
//     const title = el.find('input').first();
//     const button = el.find('button').first();
//
//     expect(title.node.ref).to.be.equal('titleRef');
//     expect(button.prop('onClick')).to.be.a('function');
//   });
//
//   it('should create a new exercise when click on the button', done => {
//     const title = 'the-title';
//
//     const onCreate = (t) => {
//       expect(t).to.be.equal(title);
//       done();
//     };
//
//     const el = shallow(<NewExercise create={onCreate} />);
//     const instance = el.instance();
//
//     instance.refs = {
//       titleRef: {value: title}
//     };
//
//     el.find('button').simulate('click');
//   });
// });
