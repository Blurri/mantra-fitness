import {describe, it} from 'mocha';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import Navigations from '../navigation/navigations.jsx';

describe('components.navigations', () => {
  it('should contain a link to plans list', () => {
    const el = shallow(<Navigations />);
    const homeLink = el.find('a').at(0);
    expect(homeLink.text()).to.be.equal('Plans');
    expect(homeLink.prop('href')).to.be.equal('/');
  });
  it('should contain a link to exercises list', () => {
    const el = shallow(<Navigations />);
    const homeLink = el.find('a').at(1);
    expect(homeLink.text().trim()).to.be.equal('Exercises');
    expect(homeLink.prop('href')).to.be.equal('/exercises');
  });

  it('should contain a link to create a new plan', () => {
    const el = shallow(<Navigations />);
    const newPlanLink = el.find('a').at(2);
    expect(newPlanLink.text().trim()).to.be.equal('New Plan');
    expect(newPlanLink.prop('href')).to.be.equal('/new-plan');
  });

  it('should contain a link to create a new exercises', () => {
    const el = shallow(<Navigations />);
    const newExerciseLink = el.find('a').at(3);
    expect(newExerciseLink.text().trim()).to.be.equal('New Exercise');
    expect(newExerciseLink.prop('href')).to.be.equal('/new-exercise');
  });
});
