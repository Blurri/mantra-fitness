import {describe, it} from 'mocha';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import Exercise from '../exercises/exercise.jsx';

describe('components.exercise', () => {
  it('should display the plan title', () => {
    const exercise = {title: 'Nice One'};
    const el = shallow(<Exercise exercise={exercise} />);
    expect(el.find('h2').text()).to.be.match(/Nice One/);
  });

  it('should display saving indicator if saving prop is there', () => {
    const exercise = {saving: true};
    const el = shallow(<Exercise exercise={exercise} />);
    expect(el.find('p').first().text()).to.be.match(/saving/i);
  });
});
