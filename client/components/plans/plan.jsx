import React from 'react';

const Plan = ({plan}) => (
  <div>
    {plan.saving ? <p>Saving...</p> : null}
    <h2>{plan.title}</h2>
    <a href={'/edit-plan/' + plan._id} className="btn btn-info" role="button">Edit</a>
  </div>
);

export default Plan;
