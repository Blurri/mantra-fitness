import React from 'react';

const PlanList = ({plans}) => (
  <div className="row">
    <div className="col-md-12">
      <table className="table table-sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {plans.map((plan, index) => (
            <tr key={plan._id}>
              <td>{index+1}</td>
              <td>{plan.title}</td>
              <td><a href={'/plans/'+plan._id}><i className="fa fa-eye"></i></a> | <a href={'/training/'+plan._id}><i className="fa fa-eye"></i></a></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </div>
);

export default PlanList;
