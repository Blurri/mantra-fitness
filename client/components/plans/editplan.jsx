import React from 'react';
import ExerciseListEditPlan from '../../containers/exercises/exerciselist.editplan';

class EditPlan extends React.Component {
  render() {
    const {error,plan} = this.props;
    return (
      <div className="edit-plan">
        {error ? <p style={{color: 'red'}}></p> : null }
        <form>
          <div className="row">
            <div className="col-xs-9">
              <fieldset className="form-group input-group-sm">
                <label>Title</label>
                <input ref="titleRef" className="form-control" type="Text" defaultValue={plan.title} placeholder="Enter your exercise title." />
              </fieldset>
            </div>
          </div>
          <hr/>
          <button className="btn" onClick={this.updatePlan.bind(this)}>Update</button>
        </form>

        <br/>
        <ExerciseListEditPlan planId={plan._id} />

      </div>
    )
  }
  updatePlan() {
    const {edit,plan} = this.props;
    const {titleRef} = this.refs;

    edit(plan._id, titleRef.value, plan.exercises);
  }

}

export default EditPlan;
