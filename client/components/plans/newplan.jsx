import React from 'react';

class NewPlan extends React.Component {
  render() {
    const { error} = this.props;
    return (
      <div className="new-plan">
        <h2>Add New Plan</h2>
        {error ? <p style={{color: 'red'}}>{error}</p> : null}
        <form>
          <fieldset className="form-group">
            <label>Plan Title</label>
            <input className="form-control" ref="titleRef" type="Text" placeholder="Enter your plan title." />
          </fieldset>
          <button className="btn btn-default" onClick={this.createPlan.bind(this)}>Add New</button>
        </form>
      </div>
    );
  }

  createPlan() {
    const {create} = this.props;
    const {titleRef} = this.refs;

    create(titleRef.value);
  }
}


export default NewPlan;
