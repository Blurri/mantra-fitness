import React from 'react';
import TrainingsCard from '../../containers/trainings/trainingscard';

class Training extends React.Component {


  render() {
    const {create,planId,training} = this.props;
    if(typeof training == 'undefined'){
      create(planId);
    }
    const today = new Date();
    return(
      <div>
        <div className="jumbotron">
          <h1 className="display-3">Hello</h1>
        </div>
        {training.exercises.map((exercise,index)=> {
          return exercise.finished ?
            ''
          :
            <TrainingsCard exerciseId={exercise.exerciseId} onClick={this.finishedExercise.bind(this, exercise.exerciseId)} key={index} />;
        })}
      </div>
    )
  }

  finishedExercise(exerciseId,event){
    event.preventDefault();
    const {addFinishedExercise,training} = this.props;
    addFinishedExercise(training._id, exerciseId);
  }
}


export default Training;
