import React from 'react';

class TrainingsCard extends React.Component {
  render() {
    const {exercise, onClick} = this.props;

    return(
      <div className="row">
        <div className="card card-block">
          <h1>{exercise.title}</h1>
          <button type="button" onClick={this.updateExercise.bind(this)} className="btn">Finished!</button>
        </div>
      </div>
    )
  }

  updateExercise(event) {
    event.preventDefault();
    const {onClick,exercise,updateForTraining} = this.props;
    updateForTraining(exercise._id, exercise.title, exercise.type, exercise.weight, exercise.duration, exercise.numberOfSets, exercise.repsInSet, exercise.description);
  }

}


export default TrainingsCard;
