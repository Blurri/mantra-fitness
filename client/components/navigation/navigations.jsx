import React from 'react';
import NavigationElement from './navigation.element.jsx';

class Navigations extends React.Component {
  render() {
      return (
        <nav className="navbar navbar-light bg-faded top-nav">
          <a className="navbar-brand" href="/">FitnessApp</a>
          <ul className="nav navbar-nav">
            <NavigationElement href="/" title="Plans" />
            <NavigationElement href="/exercises" title="Exercises" />
            <NavigationElement href="/new-plan" title="New Plan" />
            <NavigationElement href="/new-exercise" title="New Exercise" />
          </ul>
        </nav>
      )
  }
}


export default Navigations;
