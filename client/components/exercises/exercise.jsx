import React from 'react';

const Exercise = ({exercise}) => (

  <div className="jumbotron">
    {exercise.saving ? <p>Saving.....</p> : null}

    <h1 className="display-3">{exercise.title} <span className="label label-default pull-xs-right">{exercise.type}</span></h1>
    <p className="lead">{exercise.description}</p>
    <hr/>
    <div className="row">
      <div className="col-md-6">
        <div className="list-group">
          {exercise.weight > 0 ?
            <div href="#" className="list-group-item">
              <h4 className="list-group-item-heading">Weight</h4>
              <p className="list-group-item-text">{exercise.weight} / kg</p>
            </div>
            :
            null
          }
          {exercise.duration > 0 ?
            <div href="#" className="list-group-item">
              <h4 className="list-group-item-heading">Duration</h4>
              <p className="list-group-item-text">{exercise.duration} / mins</p>
            </div>
            :
            null
          }
        </div>
      </div>
      <div className="col-md-6">
        <div className="list-group">
          {exercise.numberOfSets > 0 ?
            <div href="#" className="list-group-item">
              <h4 className="list-group-item-heading">Number of Sets</h4>
              <p className="list-group-item-text">{exercise.numberOfSets}</p>
            </div>
            :
            null
          }
          {exercise.repsInSet > 0 ?
            <div href="#" className="list-group-item">
              <h4 className="list-group-item-heading">Repetition in a set</h4>
              <p className="list-group-item-text">{exercise.repsInSet}</p>
            </div>
            :
            null
          }
        </div>
      </div>
    </div>
    <hr/>
    <p className="lead">
      <a className="btn btn-info btn-lg" href={'/edit-exercise/'+ exercise._id} role="button">Edit</a>
    </p>
  </div>
);

export default Exercise;
