import React from 'react';

class EditExercise extends React.Component {
  render() {
    const {error,exercise} = this.props;
    return (
      <div className="edit-exercise">
        {error ? <p style={{color: 'red'}}>{error}</p> : null}
        <form>
          <div className="row">
            <div className="col-xs-9">
              <fieldset className="form-group input-group-sm">
                <label>Title</label>
                <input ref="titleRef" className="form-control" type="Text" defaultValue={exercise.title} placeholder="Enter your exercise title." />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Type</label>
                <select className="form-control" ref="typeRef" defaultValue={exercise.type} >
                  <option value="stammina">stammina</option>
                  <option value="strength">strength</option>
                  <option value="stretch">stretch</option>
                </select>
              </fieldset>
            </div>
          </div>
          <hr/>
          <div className="row">
            <div className="col-xs-12">
              <fieldset className="form-group input-group-sm">
                <label>description</label>
                <textarea ref="descriptionRef" className="form-control" defaultValue={exercise.description} rows="5" ></textarea>
              </fieldset>
            </div>
          </div>
          <hr/>
          <div className="row">
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Weight</label>
                <input className="form-control" type="number" defaultValue={exercise.weight} placeholder="Weight" ref="weightRef"  />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Duration</label>
                <input type="number" className="form-control" defaultValue={exercise.duration} placeholder="Duration" ref="durationRef"  />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label># of sets</label>
                <input type="number" className="form-control" defaultValue={exercise.numberOfSets} placeholder="# of Sets" ref="numberOfSetsRef" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label># of repetions in a set</label>
                <input type="number" className="form-control" defaultValue={exercise.repsInSet} placeholder="# of Sets" ref="repsInSetRef"  />
              </fieldset>
            </div>
          </div>
          <hr/>
          <button className="btn" onClick={this.updateExercise.bind(this)}>Update</button>
        </form>
      </div>
    )
  }

  updateExercise() {
    const {edit,exercise} = this.props;
    const {
      titleRef,
      typeRef,
      weightRef,
      durationRef,
      numberOfSetsRef,
      repsInSetRef,
      descriptionRef} = this.refs;
    edit(
      exercise._id,
      titleRef.value,
      typeRef.value,
      weightRef.value,
      durationRef.value,
      numberOfSetsRef.value,
      repsInSetRef.value,
      descriptionRef.value,
      exercises.plans
    );
  }
}

export default EditExercise;
