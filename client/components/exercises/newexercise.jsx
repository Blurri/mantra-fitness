// XXX: We need to find a way to use npm's react
// Otherwise, we can't run tests properly.
// To test this component, use React from npm directly
// as shown below
import React from 'react';

class NewExercise extends React.Component {
  render() {
    const {error} = this.props;
    return (
      <div className="new-exercise">
        <h2>Add New Exercise</h2>
        {error ? <p style={{color: 'red'}}>{error}</p> : null}
        <form>
          <div className="row">
            <div className="col-xs-9">
              <fieldset className="form-group input-group-sm">
                <label>Exercise Title</label>
                <input ref="titleRef" type="Text" placeholder="Enter your exercise title." className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Example select</label>
                <select className="form-control" ref="typeRef" defaultValue="stammina">
                  <option value="stammina">stammina</option>
                  <option value="strength">strength</option>
                  <option value="stretch">stretch</option>
                </select>
              </fieldset>
            </div>
          </div>

          <hr/>
          <div className="row">
            <div className="col-xs-12">
              <fieldset className="form-group input-group-sm">
                <label>Description</label>
                <textarea ref="descriptionRef" rows="5" className="form-control" ></textarea><br/>
              </fieldset>
            </div>
          </div>
          <hr/>

          <div className="row">
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Weight</label>
                <input type="number" placeholder="Weight" ref="weightRef" className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Duration</label>
                <input type="number" placeholder="Duration" ref="durationRef" className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label># of Sets</label>
                <input type="number" placeholder="# of Sets" ref="numberOfSetsRef" className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label># of repetions in a set</label>
                <input type="number" placeholder="# of Sets" ref="repsInSetRef" className="form-control" />
              </fieldset>
            </div>
          </div>
          <hr/>
          <button onClick={this.createExercise.bind(this)} className="btn">Add New</button>
        </form>
      </div>
    );
  }

  createExercise() {
    const {create} = this.props;
    const {
      titleRef,
      typeRef,
      weightRef,
      durationRef,
      numberOfSetsRef,
      repsInSetRef,
      descriptionRef} = this.refs;

    create(
      titleRef.value,
      typeRef.value,
      weightRef.value,
      durationRef.value,
      numberOfSetsRef.value,
      repsInSetRef.value,
      descriptionRef.value);
  }
}


export default NewExercise;
