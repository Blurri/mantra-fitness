import React from 'react';

const ExerciseList = ({exercises}) => (
  <div className="row">
    <div className="col-md-12">
      <table className="table table-sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {exercises.map((exercise, index) => (
            <tr key={exercise._id}>
              <td>{index+1}</td>
              <td>{exercise.title}</td>
              <td><a href={'/exercises/'+exercise._id}><i className="fa fa-eye"></i></a></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </div>
);

export default ExerciseList;
