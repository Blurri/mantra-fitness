import React from 'react';

class ExerciseListEditPlan extends React.Component {
  render() {
    const {error, exercises, plan} = this.props;
    return (
      <div className="row">
        <div className="col-md-12">
          <table className="table table-sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Type</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {exercises.map((exercise, index) => (
                <tr key={index}>
                  <td>{index+1}</td>
                  <td>{exercise.title}</td>
                  <td>{exercise.type}</td>
                  <td>
                      <button className={'btn btn-sm btn-'+ (plan.exercises.includes(exercise._id) ? 'danger':'success')}  onClick={this.addRemoveExercisesToPlan.bind(this,index)}>{plan.exercises.includes(exercise._id) ? 'remove':'add'}</button>
                      <a className="btn btn-link btn-sm" href={'/exercises/'+exercise._id}><i className="fa fa-eye"></i></a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  addRemoveExercisesToPlan(index,event) {
    const {exercises, plan, addExerciseToPlan,removeExerciseToPlan} = this.props;
    if(plan.exercises.includes(exercises[index]._id)){
      removeExerciseToPlan(plan._id, exercises[index]._id);
    }else{
      addExerciseToPlan(plan._id, exercises[index]._id);
    }
  }

}

export default ExerciseListEditPlan;
