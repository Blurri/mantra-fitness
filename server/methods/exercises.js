import {Exercises} from '/libs/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

Meteor.methods({
  'exercises.create'(_id, title, type, weight, duration, numberOfSets, repsInSet, description) {
    check(_id, String);
    check(title, String);
    check(type, String);
    check(weight, Match.Any);
    check(duration, Match.Any);
    check(numberOfSets, Match.Any);
    check(repsInSet, Match.Any);
    check(description, Match.Optional(String));

    // XXX: Do some user authorization
    const createdAt = new Date();
    const exercise = {_id,title,type,weight,duration,numberOfSets,repsInSet,description,createdAt,plans : []};
    Exercises.insert(exercise);
  },
  'exercises.update'(_id,title,type,weight,duration,numberOfSets,repsInSet,description,plans) {
    check(_id, String);
    check(title, String);
    check(type, String);
    check(weight, Match.Any);
    check(duration, Match.Any);
    check(numberOfSets, Match.Any);
    check(repsInSet, Match.Any);
    check(description, Match.Optional(String));
    const updatedAt = new Date();
    const exercise = {
      title,type,weight,duration,numberOfSets,repsInSet,description,updatedAt,plans
    };
    Exercises.update(_id, {$set : exercise})
  }
});
