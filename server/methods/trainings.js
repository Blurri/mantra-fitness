import {Trainings,Plans,Exercises} from '/libs/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

Meteor.methods({
  'trainings.create'(planId) {

    check(planId, String);
    const plan = Plans.findOne({_id : planId});
    console.log(plan);

    var exercises = [];


    for(exId of plan.exercises){
      exercises.push({exerciseId : exId, finished : false});
    }

    const createdAt = new Date();
    const training = {
      planId, createdAt,exercises : exercises
    };
    Trainings.insert(training);
  },
  'trainings.addexercisetoplan'(trainingId, exerciseId) {
    check(trainingId, String);
    check(exerciseId, String);
    const exercise = Exercises.findOne(exerciseId);
    Trainings.update(trainingId, {$addToSet : {exercises : exercise}});
  }
});
