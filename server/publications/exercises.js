import {Exercises} from '/libs/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

Meteor.publish('exercises.list', function () {
  const selector = {};
  const options = {
    fields: {_id: 1, title: 1, type : 1, plans : 1},
    sort: {createdAt: -1}
  };

  return Exercises.find(selector, options);
});

Meteor.publish('exercises.single', function (exercisesId) {
  check(exercisesId, String);
  const selector = {_id: exercisesId};
  return Exercises.find(selector);
});
