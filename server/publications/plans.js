import {Plans} from '/libs/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

Meteor.publish('plans.list', function () {
  const selector = {};
  const options = {
    fields: {_id: 1, title: 1},
    sort: {createdAt: -1}
  };

  return Plans.find(selector, options);
});



Meteor.publish('plans.single', function (planId) {
  check(planId, String);
  const selector = {_id: planId};
  return Plans.find(selector);
});
