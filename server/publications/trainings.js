import {Trainings} from '/libs/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';


Meteor.publish('trainings.singleforrun', function (planId) {
  check(planId, String);

  const selector = {planId: planId};
  return Trainings.find(selector);
});
