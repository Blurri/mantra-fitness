import <SingularEntity>List from '../../components/<PluralEntityLowerCase>/<SingularEntityLowerCase>list/index.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();
  if (Meteor.subscribe('<PluralEntityLowerCase>.list').ready()) {
    const <PluralEntityLowerCase> = Collections.<PluralEntity>.find().fetch();
    onData(null, {<PluralEntityLowerCase>});
  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(<SingularEntity>List);
