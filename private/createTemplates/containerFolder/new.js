import New<SingularEnity> from '../../components/<PluralEntityLowerCase>/new<SingularEnityLowerCase>/index.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context, clearErrors}, onData) => {
  const {LocalState} = context();
  const saving = Boolean(LocalState.get('SAVING_NEW_POST'));
  const error = LocalState.get('SAVING_ERROR');
  onData(null, {saving, error});

  // clearErrors when unmounting the component
  return clearErrors;
};

export const depsMapper = (context, actions) => ({
  create: actions.<PluralEntityLowerCase>.create,
  clearErrors: actions.<PluralEntityLowerCase>.clearErrors,
  context: () => context
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(New<SingularEnity>);
