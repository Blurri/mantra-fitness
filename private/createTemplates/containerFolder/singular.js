import <SingularEnitiy> from '../../components/<PluralEntityLowerCase>/<SingularEnitiyLowerCase>/index.jsx';
import {useDeps} from 'react-simple-di';
import {composeWithTracker, composeAll} from 'react-komposer';

export const composer = ({context, <SingularEnitiyLowerCase>Id}, onData) => {
  const {Meteor, Collections} = context();
  if (Meteor.subscribe('<PluralEntityLowerCase>.single', <SingularEnitiyLowerCase>Id).ready()) {
    const exercise = Collections.<PluralEntitity>.findOne(<SingularEnitiyLowerCase>Id);
    onData(null, {<SingularEnitiyLowerCase>});
  } else {
    onData();
  }
};

export default composeAll(
  composeWithTracker(composer),
  useDeps()
)(<SingularEnitiy>);
