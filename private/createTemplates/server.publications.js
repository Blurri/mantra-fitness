import {<PluralEntity>} from '/libs/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

Meteor.publish('<PluralEntity>.list', function () {
  const selector = {};
  const options = {
    fields: {_id: 1},
    sort: {createdAt: -1}
  };

  return <PluralEntity>.find(selector, options);
});

Meteor.publish('<PluralEntity>.single', function (<SingularEntityLowerCase>Id) {
  check(<SingularEntityLowerCase>Id, String);
  const selector = {_id: <SingularEntityLowerCase>Id};
  return <PluralEntity>.find(selector);
});
