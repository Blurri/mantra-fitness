const <SingularEnity>List = ({<PluralEntityLowerCase>}) => (
  <div>
    <ul>
      {<PluralEntityLowerCase>.map(<SingularEnityLowerCase> => (
        <li key={<SingularEnityLowerCase>._id}>
          <a href={`/<PluralEntityLowerCase>/${<SingularEnityLowerCase>._id}`}></a>
        </li>
      ))}
    </ul>
  </div>
)

export default <SingularEnity>List;
