// XXX: We need to find a way to use npm's react
// Otherwise, we can't run tests properly.
// To test this component, use React from npm directly
// as shown below
import {React} from 'meteor/react-runtime';
// import React from 'react';

class New<SingularEnity> extends React.Component {
  render() {
    const {saving, error} = this.props;
    return (
      <div className="new-<SingularEnityLowerCase>">
        <h2>Add New <SingularEnity></h2>
        {error ? <p style={{color: 'red'}}>{error}</p> : null}
        {saving ? <p>Saving.....</p> : null}

        <button onClick={this.create<SingularEnity>.bind(this)}>Add New</button>
      </div>
    )
  }

  create<SingularEnity>(){
    const {create} = this.props;
    const {} = this.refs;

    create();
  }
}


export default New<SingularEnity>;
