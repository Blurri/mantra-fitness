import {<PluralEntity>} from '/libs/collections';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

Meteor.methods({
  '<PluralEntityLowerCase>.create'(_id) {
    check(_id, String);

    // XXX: Do some user authorization
    const createdAt = new Date();
    const <SingularEntityLowerCase> = {_id, createdAt};
    <PluralEntity>.insert(<SingularEntityLowerCase>);
  }
});
