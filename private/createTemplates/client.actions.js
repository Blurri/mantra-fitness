export default {
  create({Meteor, LocalState, FlowRouter}) {
    // if (!title) {
    //   return LocalState.set('SAVING_ERROR', 'Title is required!');
    // }

    LocalState.set('SAVING_ERROR', null);
    LocalState.set('SAVING_NEW_POST', true);

    const id = Meteor.uuid();
    Meteor.call('<PluralEntityLowerCase>.create', id, (err) => {
      LocalState.set('SAVING_NEW_POST', false);
      if (err) {
        return LocalState.set('SAVING_ERROR', err.message);
      }
      FlowRouter.go(`/<PluralEntityLowerCase>/${id}`);
    });
  },

  clearErrors({LocalState}) {
    return LocalState.set('SAVING_ERROR', null);
  }
};
